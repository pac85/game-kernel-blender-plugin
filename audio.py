import os
import bpy

def conv_file(filename, base_path, audio_files_path):
    f = filename[2:]
    path, name = os.path.split(f)
    new_name = f"{audio_files_path}/{name}"
    os.system(f"cp \'{filename}\' \'{base_path}/{new_name}\'")
    return new_name

def export_speakers(base_path, audio_files_path, resources, entities, conv_transform):
    for name, speaker in bpy.data.objects.items():
        if speaker.type != 'SPEAKER':
            continue
        cid = len(resources)
        try:
            new_name = conv_file(speaker.data.sound.filepath, base_path, audio_files_path)
            resources.append({
                "filename": new_name,
                "resource_type": 'audio',
                "creation_args": {}
            })
            entities.append({
                "components": [
                        {
                            "name": "TransformComponent",
                            "creation_args": conv_transform(speaker)
                        },
                        {
                            "name": "SoundSourceComponent",
                            "creation_args": {
                                "source": {"ResourceById": cid}
                            }
                        },
                    ]
                })
        except:
            pass

