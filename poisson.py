import random
from math import *

def random_cricle():
    res = (1, 1)
    while(res[0]*res[0]+res[1]*res[1] > 1):
        res = (random.randrange(-1e5, 1e5)/1e5, random.randrange(-1e5, 1e5)/1e5)
    le = sqrt(res[0]*res[0] + res[1]*res[1])
    res = (res[0]/le, res[1]/le)
    return res

for i in range(0, 64):
    r = random_cricle()
    print("vec2("+str(r[0])+", "+str(r[1])+"),")
