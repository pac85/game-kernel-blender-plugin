import bpy
import sys
import importlib
from bpy_extras import io_utils, node_shader_utils
import os
import math
from mathutils import Vector, Matrix, Quaternion
from math import pi
import audio

modules = ['obj']
if 'DEBUG_MODE' in sys.argv:
    modules_absolute = modules
else:
    modules_absolute = ['{}.{}'.format(__name__, m) for m in modules]

for m in modules_absolute:
    if m in sys.modules:
        importlib.reload(sys.modules[m])
    else:
        globals()[m] = importlib.import_module(m)
        setattr(globals()[m], 'modules', modules_absolute)

from bpy.props import (
        BoolProperty,
        FloatProperty,
        StringProperty,
        EnumProperty,
        )
from bpy_extras.io_utils import (
        ImportHelper,
        ExportHelper,
        orientation_helper,
        path_reference_mode,
        axis_conversion,
        )



bl_info = {
    "name": "GameKernel exporter",
    "blender": (2, 80, 0),
    "category": "Object",
}

def conv_vector(v):
    return {"Float3": [v.x, v.y, v.z]}

def conv_color(c):
    return {"Float3": [c.r, c.g, c.b]}

def conv_rotation(r):
    q = r.to_quaternion()
    q1 = Quaternion([q.w, -q.x, -q.z, q.y])
    return q1.to_euler()

def conv_transform(global_matrix, axis_conv_mat, mesh):
    return {
        "position": conv_vector((global_matrix @ mesh.location) * Vector([1, 1, -1])),
        "rotation": conv_vector(conv_rotation(mesh.rotation_euler)),
        "scale"   : conv_vector(axis_conv_mat @ mesh.scale),
    }

def export_mesh(mesh_path, ob, context, global_matrix):
    depsgraph = context.evaluated_depsgraph_get()
    frame = context.scene.frame_current
    sys.modules['obj'].write_file(mesh_path, [ob], depsgraph, context.scene, True, EXPORT_NORMALS=True, EXPORT_UV=True, EXPORT_MTL=False, EXPORT_GLOBAL_MATRIX=global_matrix)

def tga_to_ktx(filename, ktx_filename, image_size, negate=False, swap_chanels=False):
    final_size = 2**(math.floor(math.log2(max(image_size))))
    #os.system('tgatoppm \'' + filename + '\' | toktx ' + ktx_filename)
    #os.system('tgatoppm \'' + filename + '\'  > /tmp/lol.ppm && toktx --genmipmap ' + ktx_filename + ' /tmp/lol.ppm')
    #os.system('toktx ' + ktx_filename + ' ' + filename)
    #os.system('convert \'' + filename + ' -resize '+ final_size + 'x' + final_size +'\\! \'  /tmp/lol.ppm && toktx --genmipmap ' + ktx_filename + ' /tmp/lol.ppm')
    toktx_opts = ' '#'--genmipmap'
    convert_options = ''
    if swap_chanels:
        convert_options += ' -color-matrix \'0 0 1\n 0 1 0\n1 0 0\' '
    if negate:
        convert_options = '-negate'
    print(f'convert \'{filename}\' -resize {final_size}x{final_size}\\! {convert_options} /tmp/lol.ppm && toktx {toktx_opts} {ktx_filename} /tmp/lol.ppm')
    os.system(f'convert \'{filename}\' -resize {final_size}x{final_size}\\! {convert_options} /tmp/lol.ppm && toktx {toktx_opts} {ktx_filename} /tmp/lol.ppm')

def conv_new_name(filepath, gkw_path, textures_path, image_size, negate = False, swap_chanels=False):
    albedo_texture = filepath[2:]
    albedo_path, albedo_name = os.path.split(albedo_texture)
    ktx_filename =  textures_path + albedo_name[:-4] + '.ktx'

    absolute_filepath = bpy.path.abspath(filepath)
    tga_to_ktx(absolute_filepath, gkw_path + '/' + ktx_filename, image_size, negate, swap_chanels)
    return ktx_filename

def get_material_atribute_path_size(mat, atribute_name):
    mat_wrap = node_shader_utils.PrincipledBSDFWrapper(mat)
    albedo_attr = getattr(mat_wrap, atribute_name, None)
    if albedo_attr is None:
        return None
    else:
        albedo_image = albedo_attr.image
    if albedo_image is None:
        return None
    return (albedo_image.filepath, albedo_image.size)

def export_scene(context, filepath, global_matrix, axis_conv_mat, use_gltf):
    if bpy.ops.object.mode_set.poll():
	            bpy.ops.object.mode_set(mode='OBJECT')

    meshes = [ob for ob in context.scene.objects if ob.type == 'MESH']
    lights = [ob for ob in context.scene.objects if ob.type == 'LIGHT']

    path, fname = os.path.split(filepath)

    gltf_filename = fname.split(".")[0] + ".glb"

    format = 'obj'
    resources = [{
            "filename": "test.ktx",
            "resource_type": "ktx",
            "creation_args": {},
        },
        {
            "filename": "testn.ktx",
            "resource_type": "ktx",
            "creation_args": {
                    "usage": {"Str": "normal"}
                },
        },
        {
            "filename": "",
            "resource_type": "material",
            "creation_args": {
                    "albedo_texture": {"ResourceById": 0},
                    "normal_map": {"ResourceById": 1},
                    "roughness_map": {"Float3": [0.5, 0.5, 0.5]},
                    "metalness_map": {"Float3": [0.0, 0.5, 0.5]}
                }
        }
    ]

    if use_gltf:
        resources.append({
                    "filename": gltf_filename,
                    "resource_type": "gltf",
                    "creation_args": {}
                })

        #doing it there prevents other writes from working
        bpy.ops.export_scene.gltf(filepath=path+'/'+gltf_filename)


    text_dir = '/textures/'
    try:
        os.mkdir(path + text_dir)
    except FileExistsError:
        pass

    mat_dict = {}
    for mat_name, mat in bpy.data.materials.items():
        try:
            (albedo_filepath, albedo_size) = get_material_atribute_path_size(mat, "base_color_texture")
        except TypeError:
            (albedo_filepath, albedo_size) = (None, None)

        try:
            (normal_filepath, normal_size) = get_material_atribute_path_size(mat, "normalmap_texture")
        except TypeError:
            normal_filepath = None

        try:
            (roughness_filepath, roughness_size) = get_material_atribute_path_size(mat, "roughness_texture")
        except TypeError:
            roughness_filepath = None

        try:
            (metallic_filepath, metallic_size) = get_material_atribute_path_size(mat, "metallic_texture")
        except TypeError:
            metallic_filepath = None

        if albedo_filepath is None:
            pass
        else:
            albedo_ktx_filename = conv_new_name(albedo_filepath, path, text_dir[1:], albedo_size)
        normal_id = 1
        roughness_id = 1
        if normal_filepath is None:
            pass
        else:
            normal_ktx_filename = conv_new_name(normal_filepath, path, text_dir[1:], normal_size)
            resources.append(
                {
                    "filename": normal_ktx_filename,
                    "resource_type": "ktx",
                    "creation_args": {
                            "usage": {"Str": "normal"}
                        },
                }
            )
            normal_id = len(resources) - 1

        if roughness_filepath is not None:
            if max(roughness_size) == 0:
                roughness_filepath = None
            else:
                roughness_ktx_filename = conv_new_name(roughness_filepath, path, text_dir[1:], roughness_size, True)
                resources.append(
                    {
                        "filename": roughness_ktx_filename,
                        "resource_type": "ktx",
                        "creation_args": {
                                "usage": {"Str": "roughness"}
                            },
                    }
                )
                roughness_id = len(resources) - 1

        if metallic_filepath is not None:
            if max(metallic_size) == 0:
                metallic_filepath = None
            else:
                metallic_ktx_filename = conv_new_name(metallic_filepath, path, text_dir[1:], metallic_size, True)
                resources.append(
                    {
                        "filename": roughness_ktx_filename,
                        "resource_type": "ktx",
                        "creation_args": {
                                "usage": {"Str": "metalness"}
                            },
                    }
                )
                metalness_id = len(resources) - 1

        if albedo_filepath is not None:
            resources.append(
                {
                    "filename": albedo_ktx_filename,
                    "resource_type": "ktx",
                    "creation_args": {},
                }
            )
            albedo_j = {"ResourceById": len(resources) - 1}
        else:
            try:
                albedo_j = conv_color(node_shader_utils.PrincipledBSDFWrapper(mat).base_color)
            except:
                albedo_j = {"Float3": [1.0, 0.0, 0.0]}

        if roughness_filepath is not None:
            roughness_map = {"ResourceById": roughness_id}
        else:
            roughness_value = node_shader_utils.PrincipledBSDFWrapper(mat).roughness.real
            roughness_map = {"Float3": [roughness_value, roughness_value, roughness_value]}

        if metallic_filepath is not None:
            metalness_map = {"ResourceById": metalness_id}
        else:
            metalness_value = node_shader_utils.PrincipledBSDFWrapper(mat).metallic.real
            metalness_map = {"Float3": [metalness_value, metalness_value, metalness_value]}

        resources.append(
            {
                "filename": "",
                "resource_type": "material",
                "creation_args": {
                        "albedo_texture": albedo_j,
                        "normal_map": {"ResourceById": normal_id},
                        "roughness_map": roughness_map,
                        "metalness_map": metalness_map
                    }
            }
        )

        mat_dict[mat_name] = len(resources) - 1

    mesh_dir = path + '/meshes/'
    try:
        os.mkdir(mesh_dir)
    except FileExistsError:
        pass

    cres = len(resources)

    for mesh in meshes:
        mesh_path = mesh_dir + mesh.name + '.' + format

        if not use_gltf:
            export_mesh(mesh_path, mesh, context, global_matrix)

            resources.append({
                "filename": 'meshes/' + mesh.name + '.' + format,
                "resource_type": format,
                "creation_args": {}
            })
        else:
            resources.append({
                "filename": '',
                "resource_type": "gltf_mesh",
                "creation_args": {
                        "gltf": { "ResourceById": 3 }, #hardoced gltf index
                        "node_name": { "Str": mesh.name }
                    }
            })

    entities = []
    for mesh in meshes:
        material_id = 2
        try:
            material_name = mesh.material_slots[0].name
            material_id = mat_dict[material_name]
        except:
            pass

        #bpy.data.objects['sponza_286'].material_slots.keys()[0]
        entities.append({
            "components": [
                {
                    "name": "TransformComponent",
                    "creation_args": conv_transform(global_matrix, axis_conv_mat, mesh)
                },
                {
                    "name": "StaticMeshComponent",
                    "creation_args": {
                            "material": {"ResourceById": material_id},
                            "mesh": {"ResourceById": cres}
                        }
                },
            ]
        })
        cres += 1

    for light in lights:
        if light.data.type == "POINT":
            resources.append({
                    "filename": "",
                    "resource_type": "point_light",
                    "creation_args": {
                            "color": {
                                "Float3": [
                                    light.data.color.r*light.data.energy,
                                    light.data.color.g*light.data.energy,
                                    light.data.color.b*light.data.energy
                                ]
                            },
                            "position": {"Float3": [0.0, 0.0, 0.0]},
                            "size": {"Float": light.data.shadow_soft_size},
                            "influence_radius": {"Float": light.data.cutoff_distance}
                        }
                })
            entities.append({
                "components": [
                        {
                            "name": "TransformComponent",
                            "creation_args": conv_transform(global_matrix, axis_conv_mat, light)
                        },
                        {
                            "name": "LightComponent",
                            "creation_args": {
                                "light": {"ResourceById": cres}
                            }
                        },
                    ]
                })
        elif light.data.type == "SPOT":
            angle = light.rotation_euler.to_matrix() @ Vector([0.0, -1.0, 0.0])
            creation_args = {
                            "color": {
                                "Float3": [
                                    light.data.color.r*light.data.energy,
                                    light.data.color.g*light.data.energy,
                                    light.data.color.b*light.data.energy
                                ]
                            },
                            "position": {"Float3": [0.0, 0.0, 0.0]},
                            "direction": conv_vector(angle),
                            "size": {"Float": light.data.shadow_soft_size},
                            "cone_length": {"Float": light.data.cutoff_distance},
                            "cone_angle": {"Float": light.data.spot_size / pi * 180.0}
                        }
            if light.data.use_shadow:
                shadow_map_size = light.data.custom_props.shadow_map_size
                if shadow_map_size is None:
                    shadow_map_size = 512
                else:
                    shadow_map_size = int(shadow_map_size)
                creation_args = dict(creation_args, **{"shadow_map_size": {"Uint": shadow_map_size}})

            resources.append({
                    "filename": "",
                    "resource_type": "spot_light",
                    "creation_args": creation_args
                })
            entities.append({
                "components": [
                        {
                            "name": "TransformComponent",
                            "creation_args": conv_transform(global_matrix, axis_conv_mat, light)
                        },
                        {
                            "name": "LightComponent",
                            "creation_args": {
                                "light": {"ResourceById": cres}
                            }
                        },
                    ]
                })
        cres += 1


    audio_dir = '/audio/'
    try:
        os.mkdir(path + audio_dir)
    except FileExistsError:
        pass

    audio.export_speakers(path, audio_dir, resources, entities, lambda ob: conv_transform(global_matrix, axis_conv_mat, ob))

    import json
    gkw = json.dumps(
        {
            "resources": resources,
            "entities": entities,
        }
    )

    f = open(filepath, "w")
    f.write(gkw)
    f.close()

    #if use_gltf:
        #bpy.ops.export_scene.gltf(filepath=path+'/'+gltf_filename)

@orientation_helper(axis_forward='-Z', axis_up='Y')
class ExportGkw(bpy.types.Operator, ExportHelper):
    """Export as GameKernel map"""

    bl_idname = "export_scene.gkw"
    bl_label = 'Export gkw'
    bl_options = {'PRESET'}

    filename_ext = ".gkw.json"
    filter_glob: StringProperty(
            default="*.gkw.json",
            options={'HIDDEN'},
            )

    # object group
    use_mesh_modifiers: BoolProperty(
            name="Apply Modifiers",
            description="Apply modifiers",
            default=True,
            )
    # Non working in Blender 2.8 currently.
    # ~ use_mesh_modifiers_render: BoolProperty(
            # ~ name="Use Modifiers Render Settings",
            # ~ description="Use render settings when applying modifiers to mesh objects",
            # ~ default=False,
            # ~ )

    use_smooth_groups: BoolProperty(
            name="Smooth Groups",
            description="Write sharp edges as smooth groups",
            default=False,
            )
    use_smooth_groups_bitflags: BoolProperty(
            name="Bitflag Smooth Groups",
            description="Same as 'Smooth Groups', but generate smooth groups IDs as bitflags "
                        "(produces at most 32 different smooth groups, usually much less)",
            default=False,
            )
    use_normals: BoolProperty(
            name="Write Normals",
            description="Export one normal per vertex and per face, to represent flat faces and sharp edges",
            default=True,
            )
    use_uvs: BoolProperty(
            name="Include UVs",
            description="Write out the active UV coordinates",
            default=True,
            )
    use_materials: BoolProperty(
            name="Write Materials",
            description="Write out the MTL file",
            default=True,
            )
    use_nurbs: BoolProperty(
            name="Write Nurbs",
            description="Write nurbs curves as OBJ nurbs rather than "
                        "converting to geometry",
            default=False,
            )
    use_vertex_groups: BoolProperty(
            name="Polygroups",
            description="",
            default=False,
            )

    # grouping group
    use_blen_objects: BoolProperty(
            name="OBJ Objects",
            description="Export Blender objects as OBJ objects",
            default=True,
            )
    group_by_object: BoolProperty(
            name="OBJ Groups",
            description="Export Blender objects as OBJ groups",
            default=False,
            )
    group_by_material: BoolProperty(
            name="Material Groups",
            description="Generate an OBJ group for each part of a geometry using a different material",
            default=False,
            )
    keep_vertex_order: BoolProperty(
            name="Keep Vertex Order",
            description="",
            default=False,
            )

    global_scale: FloatProperty(
            name="Scale",
            min=0.01, max=1000.0,
            default=1.0,
            )

    path_mode: path_reference_mode

    check_extension = True

    def execute(self, context):
        #from . import export_obj

        from mathutils import Matrix
        keywords = self.as_keywords(ignore=("axis_forward",
                                            "axis_up",
                                            "global_scale",
                                            "check_existing",
                                            "filter_glob",
                                            ))

        axis_conv_mat = axis_conversion(to_forward=self.axis_forward,
                                         to_up=self.axis_up,
                                         ).to_4x4()
        global_matrix = (Matrix.Scale(self.global_scale, 4) @
                         axis_conv_mat)

        print(global_matrix)
        keywords["global_matrix"] = global_matrix
        export_scene(context, self.filepath, global_matrix, axis_conv_mat, True)
        return {'FINISHED'}

    def draw(self, context):
        pass

from bpy.props import (IntProperty)
from bpy.types import (AnyType, PropertyGroup)

class LightPropertyGroup(PropertyGroup):
    shadow_map_size: IntProperty(
            name = "size of the shadow map",
            description = "",
            default = 512
            )

class GameKernelPanel(bpy.types.Panel):
    bl_idname = "OBJECT_PT_gkp"
    bl_label = "GameKernel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"

    sm_size_p = IntProperty(
            name = "size of the shadow map",
            description = "",
            default = 512
            )
    sm_size = 512

    @classmethod
    def poll(cls, context):
        return context.object.type == 'LIGHT'

    def draw(self, context):
        self.layout.label(text="test")
        #context.object.custom_props = bpy.props.PointerProperty(type=LightPropertyGroup)
        self.layout.prop(context.object.data.custom_props, "shadow_map_size")

def menu_func_export(self, context):
	    self.layout.operator(ExportGkw.bl_idname, text="GameKernel (.gkw.json)")

def register():
    bpy.utils.register_class(ExportGkw)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
    bpy.utils.register_class(LightPropertyGroup)
    bpy.types.Light.custom_props  = bpy.props.PointerProperty(type=LightPropertyGroup)
    bpy.utils.register_class(GameKernelPanel)

def unregister():
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
    bpy.utils.unregister_class(ExportGkw)

if __name__ == "__main__":
    register()
